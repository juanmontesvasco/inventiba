package util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.juanmontes.inventiba.R
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStreamReader

class Util(private val context: Context) {

    fun save(fileName: String, fileData: String) {
        val file: String = fileName
        val data: String = fileData
        val fileOutputStream: FileOutputStream
        try {
            fileOutputStream = context.openFileOutput(file, Context.MODE_PRIVATE)
            fileOutputStream.write(data.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun get(name: String): String {
        if (name != null && name.trim() != "") {
            var fileInputStream: FileInputStream? = null
            fileInputStream = context.openFileInput(name)
            var inputStreamReader: InputStreamReader = InputStreamReader(fileInputStream)
            val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder: StringBuilder = StringBuilder()
            var text: String? = null
            while ({ text = bufferedReader.readLine(); text }() != null) {
                stringBuilder.append(text)
            }
            return stringBuilder.toString()
        }
        return ""
    }

    fun requestError() {
        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_LONG).show()
    }

    fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun validateSession(): Boolean {
        return try {
            get(Parameters.USERNAME).isNotEmpty()
        } catch (e: Exception) {
            false
        }
    }

    fun login(username: String) {
        save(Parameters.USERNAME, username)
    }

    fun logout() {
        save(Parameters.USERNAME, "")
    }

    fun alert(title: String?, message: String?, button: String?) {
        try {
            if (!(context as Activity).isFinishing) {
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.alert)
                dialog.setTitle(title)
                val messageText = dialog.findViewById<View>(R.id.alert_text) as TextView
                messageText.text = message
                dialog.setCancelable(false)
                dialog.show()
                val accept = dialog.findViewById<View>(R.id.accept) as Button
                accept.text = button
                accept.setOnClickListener { dialog.dismiss() }
            }
        } catch (e: java.lang.Exception) {
        }
    }
}