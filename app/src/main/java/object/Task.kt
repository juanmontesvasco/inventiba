package `object`

import org.json.JSONObject

class Task(task: JSONObject) {
    var userId: Int = task.getInt("userId")
    var id: Int = task.getInt("id")
    var title: String = task.getString("title")
    var completed: Boolean = task.getBoolean("completed")
}