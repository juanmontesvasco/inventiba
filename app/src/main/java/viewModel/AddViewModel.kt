package viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AddViewModel : ViewModel() {

    private val _successAdd = MutableLiveData<Boolean>()
    val successAdd: LiveData<Boolean> get() = _successAdd

    fun onButtonClicked(userId: String, taskId: String, title: String) {
        _successAdd.value = userId.isNotEmpty() && taskId.isNotEmpty() && title.isNotEmpty()
    }
}