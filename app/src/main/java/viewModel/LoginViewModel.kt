package viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel : ViewModel() {

    private val _successLogin = MutableLiveData<Boolean>()
    val successLogin: LiveData<Boolean> get() = _successLogin

    fun onButtonClicked(username: String, password: String) {
        _successLogin.value = username.isNotEmpty() && password.isNotEmpty()
    }
}