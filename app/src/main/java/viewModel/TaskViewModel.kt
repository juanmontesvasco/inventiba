package viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.Response
import com.android.volley.VolleyError
import network.Request
import org.json.JSONArray
import util.Parameters
import java.lang.Exception

class TaskViewModel : ViewModel() {

    private val _tasks = MutableLiveData<MutableList<`object`.Task>>()
    val tasks: LiveData<MutableList<`object`.Task>> get() = _tasks

    private val _error = MutableLiveData<VolleyError>()
    val error: LiveData<VolleyError> get() = _error

    private val _progressVisible = MutableLiveData<Boolean>()
    val progressVisible: LiveData<Boolean> get() = _progressVisible

    fun getPost(context: Context) {
        val request = Request(context)
        _progressVisible.value = true
        val success = Response.Listener<String> { response ->
            _progressVisible.value = false
            try {
                val json = JSONArray(response)
                var listTask: MutableList<`object`.Task> = ArrayList()
                for (i in 0 until json.length()) {
                    var task = `object`.Task(json.getJSONObject(i))
                    listTask.add(task)
                }
                _tasks.value = listTask
            } catch (e: Exception) {
                _error.value = VolleyError()
            }
        }

        val error = Response.ErrorListener { error ->
            _error.value = error
            _progressVisible.value = false
        }

        request.getHttp(
            Parameters.SERVER,
            request.getHeaders(),
            success,
            error
        )
    }
}