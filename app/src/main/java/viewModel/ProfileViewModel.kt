package viewModel

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.juanmontes.inventiba.R

class ProfileViewModel : ViewModel() {


    private val _logout = MutableLiveData<Boolean>()
    val logout: LiveData<Boolean> get() = _logout


    fun onLogoutClicked(
        title: String?,
        message: String?,
        context: Context
    ) {
        try {
            if (!(context as Activity).isFinishing) {
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.confirm_alert)
                dialog.setTitle(title)
                val messageText = dialog.findViewById<View>(R.id.alert_text) as TextView
                messageText.text = message
                dialog.setCancelable(false)
                dialog.show()
                val accept = dialog.findViewById<View>(R.id.confirm) as Button
                accept.setOnClickListener {
                    dialog.dismiss()
                    _logout.value = true
                }
                val cancel = dialog.findViewById<View>(R.id.decline) as Button
                cancel.setOnClickListener {
                    dialog.dismiss()
                    _logout.value = false
                }
            }
        } catch (e: java.lang.Exception) {
        }
    }
}