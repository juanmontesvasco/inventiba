package com.juanmontes.inventiba

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.juanmontes.inventiba.databinding.ActivityLoginBinding
import util.Util
import viewModel.LoginViewModel


class Login : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel
    private lateinit var util: Util
    private val PERMS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val REQUEST = 810

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObjects()
        verifyPermissions()
        validateSession()
        setUpView()
        observe()
    }

    private fun initObjects() {
        viewModel = ViewModelProvider(this).get()
        util = Util(this)
    }

    private fun setUpView() {
        with(binding) {
            login.setOnClickListener {
                viewModel.onButtonClicked(username.text.toString(), password.text.toString())
            }
        }
    }

    private fun observe() {
        viewModel.successLogin.observe(this, { success ->
            if (success) {
                successLogin()
            } else {
                failedLogin()
            }
        })
    }

    private fun successLogin() {
        util.toast(getString(R.string.welcome))
        util.login(binding.username.text.toString())
        goToMain()
    }

    private fun failedLogin() {
        util.toast(getString(R.string.session_error))
    }

    private fun goToMain() {
        val intent = Intent(this, Main::class.java).apply {
        }
        startActivity(intent)
        finish()
    }

    private fun validateSession() {
        if (util.validateSession()) {
            goToMain()
        }
    }

    private fun hasPermission(perm: String): Boolean {
        return try {
            PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm)
        } catch (e: java.lang.Exception) {
            true
        }
    }

    private fun canAccessWrite(): Boolean {
        return hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST) {
            if (!canAccessWrite()) {
                util.toast(getString(R.string.permissions))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun verifyPermissions() {
        if (!canAccessWrite()) {
            requestPermissions(
                PERMS,
                REQUEST
            )
        }
    }
}