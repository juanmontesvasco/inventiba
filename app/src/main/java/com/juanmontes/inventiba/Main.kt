package com.juanmontes.inventiba

import adapter.PageAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.juanmontes.inventiba.databinding.ActivityMainBinding

class Main : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupActionBar()
    }

    private fun setupActionBar() {
        with(binding) {
            viewpager.adapter = PageAdapter(supportFragmentManager, this@Main)
            tabs.setupWithViewPager(viewpager)
        }
    }
}