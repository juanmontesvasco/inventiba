package adapter

import `object`.Task
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.juanmontes.inventiba.R

class TaskAdapter : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    var tasks: MutableList<Task> = ArrayList()
    private lateinit var context: Context

    fun TaskAdapter(tasks: MutableList<Task>, context: Context) {
        this.tasks = tasks
        this.context = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tasks[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_task, parent, false))
    }

    override fun getItemCount(): Int {
        return tasks.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val description = view.findViewById(R.id.description) as TextView
        private val complete = view.findViewById(R.id.complete) as ImageView
        private val incomplete = view.findViewById(R.id.incomplete) as ImageView


        fun bind(task: Task) {
            description.text = "${task.title}"
            if (task.completed) {
                incomplete.visibility = View.GONE
                complete.visibility = View.VISIBLE
            } else {
                complete.visibility = View.GONE
                incomplete.visibility = View.VISIBLE
            }
        }
    }
}