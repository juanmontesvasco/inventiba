package adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.juanmontes.inventiba.R
import fragment.Add
import fragment.Profile
import fragment.Task

class PageAdapter(fm: FragmentManager, val context: Context) : FragmentPagerAdapter(fm) {


    override fun getCount(): Int {
        return 3;
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                Task()
            }
            1 -> {
                Add()
            }
            2 -> {
                Profile()
            }
            else -> {
                Task()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> {
                return context.getString(R.string.tasks)
            }
            1 -> {
                return context.getString(R.string.add)
            }
            2 -> {
                return context.getString(R.string.profile)
            }
        }
        return super.getPageTitle(position)
    }

}