package fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.juanmontes.inventiba.Login
import com.juanmontes.inventiba.R
import com.juanmontes.inventiba.databinding.FragmentProfileBinding
import util.Parameters
import util.Util
import viewModel.ProfileViewModel

class Profile : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var util: Util
    private lateinit var viewModel: ProfileViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        initObjects()
        setUpView()
        observe()
        return binding.root
    }


    private fun initObjects() {
        util = Util(requireActivity())
        viewModel = ViewModelProvider(this).get()
    }

    private fun setUpView() {
        with(binding) {
            usernameProfile.text = util.get(Parameters.USERNAME)
            logout.setOnClickListener {
                viewModel.onLogoutClicked(
                    getString(R.string.app_name),
                    getString(R.string.confirm_logout),
                    requireActivity()
                )
            }
        }
    }

    private fun observe() {
        viewModel.logout.observe(viewLifecycleOwner, { success ->
            if (success) {
                logout()
            }
        })
    }

    private fun logout() {
        util.toast(getString(R.string.see_you))
        util.logout()
        val intent = Intent(requireActivity(), Login::class.java).apply {
        }
        startActivity(intent)
        requireActivity().finish()
    }


}