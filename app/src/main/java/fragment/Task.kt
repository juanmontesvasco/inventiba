package fragment

import adapter.TaskAdapter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.juanmontes.inventiba.R
import com.juanmontes.inventiba.databinding.FragmentTaskBinding
import util.Util
import viewModel.TaskViewModel


class Task : Fragment() {

    private lateinit var binding: FragmentTaskBinding
    private lateinit var util: Util
    private lateinit var viewModel: TaskViewModel
    private val mAdapter: TaskAdapter = TaskAdapter()
    private lateinit var listTasks: MutableList<`object`.Task>


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTaskBinding.inflate(inflater, container, false)
        initObjects()
        observe()
        viewModel.getPost(requireActivity())
        return binding.root
    }


    private fun initObjects() {
        util = Util(requireActivity())
        viewModel = ViewModelProvider(this).get()
        listTasks = ArrayList()
    }

    private fun observe() {
        viewModel.tasks.observe(viewLifecycleOwner, { tasks ->
            listTasks = tasks
            showData()
        })

        viewModel.error.observe(viewLifecycleOwner, {
            util.toast(getString(R.string.error))
        })

        viewModel.progressVisible.observe(viewLifecycleOwner, { show ->
            if (show) {
                binding.progress.visibility = View.VISIBLE
            } else {
                binding.progress.visibility = View.GONE
            }
        })
    }

    private fun showData() {
        with(binding) {
            mAdapter.TaskAdapter(listTasks, requireActivity())
            recyclerTasks.adapter = mAdapter
        }
    }

}