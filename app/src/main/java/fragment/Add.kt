package fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.juanmontes.inventiba.R
import com.juanmontes.inventiba.databinding.FragmentAddBinding
import util.Util
import viewModel.AddViewModel

class Add : Fragment() {

    private lateinit var binding: FragmentAddBinding
    private lateinit var util: Util
    private lateinit var viewModel: AddViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddBinding.inflate(inflater, container, false)
        initObjects()
        setUpView()
        observe()
        return binding.root
    }


    private fun initObjects() {
        util = Util(requireActivity())
        viewModel = ViewModelProvider(this).get()
    }


    private fun setUpView() {
        with(binding) {
            add.setOnClickListener {
                viewModel.onButtonClicked(
                    userId.text.toString(),
                    taskId.text.toString(),
                    title.text.toString()
                )
            }
        }
    }

    private fun observe() {
        viewModel.successAdd.observe(viewLifecycleOwner, { success ->
            if (success) {
                successAdd()
            } else {
                failedAdd()
            }
        })
    }

    private fun successAdd() {
        clearFields()
        binding.userId.requestFocus()
        util.alert(
            getString(R.string.app_name),
            getString(R.string.success_add),
            getString(R.string.accept)
        )
    }

    private fun failedAdd() {
        util.toast(getString(R.string.failed_add))
    }


    private fun clearFields() {
        with(binding) {
            userId.text?.clear()
            taskId.text?.clear()
            title.text?.clear()
            finished.isChecked = false
        }
    }

}